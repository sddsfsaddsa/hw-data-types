function convert(...args) {
    return args.map((arg) => {
        if (typeof arg === 'string') {
            return Number(arg);
        } else if (typeof arg === 'number') {
            return arg.toString();
        }
    });
}

const executeforEach = (array, cb) => {
    array.forEach(element => {
        cb(element);
    })
};

const mapArray = (array, cb) => array.map(element => cb(Number(element)));

const filterArray = (array, cb) => array.filter(element => cb(Number(element)));

const flipOver = (str) => str.split("").reverse().join("");

const makeListFromRange = (array) => {
    let resArray = [];
    if (array.length === 1) return array;
    for (let i = array[0]; i <= array[1]; i++) {
        resArray.push(i);
    }
    return resArray;
};

const getArrayOfKeys = (array, property) => array.map(element => element[property]);

const substitute = (array) => mapArray(array, (number) => number > 30 ? number : "*");

const getPastDay = (date, number) => {
    let dateInMiliseconds = date.getMilliseconds();
    let daysMs = number * 86400000;
    return new Date(dateInMiliseconds - daysMs + 86400000).getDate();
};

const formatDate = (dat) => {
    return dat.toLocaleString("en-ZA").slice(0,-3).replace(",","");
};

module.exports = {
    convert,
    executeforEach,
    mapArray,
    filterArray,
    flipOver,
    makeListFromRange,
    getArrayOfKeys,
    substitute,
    getPastDay,
    formatDate,
};